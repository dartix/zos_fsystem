//
// Created by daniel on 17.10.2020.
// testovaci soubor

#include <iostream>

#include <string_view>
#include "file_system.h"

std::vector<std::string> parse_cmd(const std::string& str) {
    std::istringstream iss(str);
    std::vector<std::string> ret;

    std::string tmp;
    while (std::getline(iss, tmp, ' '))
        ret.push_back(tmp);

    return ret;
}

int calculateBytes(const std::string& value) {
    int retVal;
    try {
        retVal = std::stoi(value);
    }
    catch (std::invalid_argument &ex) {
        return 0;
    }
    if (value.substr(value.size() - 2, 2) == "KB")
        return retVal;
    if (value.substr(value.size() - 2, 2) == "MB")
        return retVal * 1000;
    if (value.substr(value.size() - 2, 2) == "GB")
        return retVal * 1000000;
    return 0;
}

bool consumeCommand(const std::string& command, const std::string& prompt, std::unique_ptr<FileSystem> &fileSystem,
                    const std::string& arg) {
    std::string_view sv(command);

    if (sv == "exit" || sv == "quit") {
        std::cout << "Quit? ok..." << std::endl;
        return false;
    }
    else if (sv.substr(0, 6) == "format") {
        std::cout << "format? ok..." << std::endl;
        std::vector<std::string> parsed = parse_cmd(command);
        int val = calculateBytes(parsed[1]);
        if (val > 9) // minimální velikost je 9KB
            fileSystem = std::make_unique<FileSystem>(val * 1024, arg);
        else std::cout << "INVALID COMMAND" << std::endl;
    }
    else if (sv.substr(0, 4) == "open") {
        fileSystem = std::make_unique<FileSystem>(arg, true);
    }
    else if (sv.substr(0, 3) == "pwd"){
        std::cout << fileSystem->pwd() << std::endl;
    }
    else if (sv.substr(0, 2) == "ls"){
        fileSystem->ls();
    }
    else if (sv.substr(0, 2) == "cd"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->cd(parsed[1], true);
    }
    else if (sv.substr(0, 5) == "mkdir"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->mkdir(parsed[1]);
    }
    else if (sv.substr(0, 4) == "stat"){
        fileSystem->printFullBitIndexes();
    }
    else if (sv.substr(0, 5) == "rmdir"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->rmdir(parsed[1]);
    }
    else if (sv.substr(0, 4) == "info"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->info(parsed[1]);
    }
    else if (sv.substr(0, 4) == "incp"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->incp(parsed[1], parsed[2]);
    }
    else if (sv.substr(0, 5) == "outcp"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->outcp(parsed[1], parsed[2]);
    }
    else if (sv.substr(0, 3) == "cat"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->cat(parsed[1]);
    }
    else if (sv.substr(0, 2) == "rm"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->rm(parsed[1]);
    }
    else if (sv.substr(0, 2) == "ln"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->ln(parsed[1], parsed[2]);
    }
    else if (sv.substr(0, 2) == "cp"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->cp(parsed[1], parsed[2]);
    }
    else if (sv.substr(0, 2) == "mv"){
        std::vector<std::string> parsed = parse_cmd(command);
        fileSystem->mv(parsed[1], parsed[2]);
    }

    else {
        std::cout << "INVALID COMMAND" << std::endl;
    }
    return true;

}

std::vector<std::string> load(const std::string& path) {

    std::vector<std::string> cmds;
    std::ifstream file(path);
    if (!file) {
        std::cout << "FILE NOT FOUND" << std::endl;
        cmds.clear();
        return cmds;
    }

    std::string tmp;
    while (std::getline(file, tmp))
        cmds.push_back(tmp);

    return cmds;

}

int main(int argc, char **argv) {

    std::string command;
    std::string prompt = "i-FS> ";
    std::string arg;

    if (argc < 2) {
        std::cout << "NO PARAMETERS... trying open ./fs_disk.img" << std::endl;
         arg = "./fs_disk.img";
    }
    else arg = argv[1];

    std::unique_ptr<FileSystem> fileSystem = std::make_unique<FileSystem>(arg, false);

    std::cout << prompt;

    while (std::getline(std::cin, command)) {



        if (command.substr(0, 4) == "load"){
            std::vector<std::string> parsed = parse_cmd(command);
            parsed = load(parsed[1]);
            if (!parsed.empty()) {
                for (auto &cmd: parsed) {
                    std::cout << cmd << std::endl;
                    if(!consumeCommand(cmd, prompt, fileSystem, arg))
                        return 0;
                }
                std::cout << std::endl << "load OK" << std::endl << std::endl;
            }

            std::cout << prompt;
        }
        else {
            if(!consumeCommand(command, prompt, fileSystem, arg))
                break;
            std::cout << prompt;
        }

    }




    return 0;
}