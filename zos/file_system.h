//
// Created by daniel on 11.12.2020.
//

#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H

#include <iostream>
#include <cstdint>
#include <fstream>
#include <memory>
#include <vector>
#include <utility>
#include <stack>
#include <sstream>
#include "fs_structs.h"



class FileSystem {

private:

    /**
     * kořenový adresář
     */
    pseudo_inode root{};

    /**
     * aktuální adresář
     */
    pseudo_inode current{};


public:

    /**
     * Vytvoří souborový systém (příkaz format)
     * @param size velikost disku [Bytes]
     * @param path cesta k souboru
     */
    FileSystem(int32_t size, std::string path);

    /**
     * Otevře souborový systém (open)
     * @param path cesta k souboru
     */
    explicit FileSystem(std::string path, bool info);

    /**
     * vypíše cestu k aktuálnímu adresáři
     * @return cesta
     */
    std::string pwd();

    /**
     * vytvoří adresář
     * @param name jméno nebo cesta se jménem
     */
    void mkdir(const std::string& name);

    /**
     * přesune se do daného adresáře
     * @param path cesta
     * @param stay zuůstat v daném adresáři?
     * @return pokud nezůstane v adresáři, vrátí jeho inode
     */
    pseudo_inode cd(const std::string& path, bool stay);

    /**
     * vypíše obsah adresáře
     */
    void ls();

    /**
     * vypíše plné indexy u bitmapy dat a inodů
     */
    void printFullBitIndexes();

    /**
     * smaže prázdný adrsář
     * @param path cesta k adresáři nebo jméno
     */
    void rmdir(const std::string& path);

    /**
     * zobrazí info o souboru či složce
     * @param path cesta nebo název
     */
    void info(const std::string& path);

    /**
     * Nahraje soubor s1 z pevného disku do umístění s2 v i-FS
     * @param source s1 z pevného disku
     * @param dest umístění s2
     */
    void incp(const std::string& source, const std::string& dest);

    /**
     * vypíše obsah daného souboru
     * @param path cesta k danému souboru
     */
    void cat(const std::string& path);

    /**
     * Nahraje soubor s1 z i-FS do umístění s2 na pevném disku
     * @param source  s1 z i-FS
     * @param dest  umístění s2
     */
    void outcp(const std::string& source, const std::string& dest);

    /**
     * smaže soubor
     * @param path cesta k souboru
     */
    void  rm(const std::string& path);

    /**
     * Přesune soubor s1 do umístění s2, nebo přejmenuje s1 na s2
     * @param source s1 zdroj
     * @param dest s2 cíl
     */
    void mv(const std::string& source, const std::string& dest);

    /**
     * zkopíruje soubor
     * @param source zdroj
     * @param dest cíl
     */
    void cp(const std::string& source, const std::string& dest);

    /**
     * hardlink
     * @param old "starý" soubor
     * @param newLink nový soubor
     */
    void ln(const std::string& old, const std::string& newLink);

/**
 * načtený superblock
 */
superblock super_block{};
/**
 * cesta k souboru na opravdovém fs
 */
std::string fs_path;
};


#endif //FILE_SYSTEM_H
