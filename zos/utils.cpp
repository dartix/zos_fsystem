//
// Created by daniel on 07.01.2021.
//

#include "utils.h"


directory_item createDirectoryItem(int32_t node_id, std::string item_name){
    auto dir = directory_item{};
    dir.inode_id = node_id;
    std::copy(item_name.begin(), item_name.end() + '\0', dir.item_name);
    return dir;
}

int32_t getINodeLocation(FileSystem *fileSystem, int32_t nodeId) {
    return fileSystem->super_block.inode_start_address + (nodeId - 1) * sizeof(pseudo_inode);

}

std::vector<std::string> parsePath(const std::string& path){
    // parsovani cesty
    std::vector<std::string> spath;

    try {
        std::istringstream iss(path);
        std::string tmp;



        while (std::getline(iss, tmp, '/')) {
            spath.push_back(tmp);
        }

    }
    catch (std::exception &exception) {
        spath.clear();
        spath.emplace_back("NULL!");
        return spath;
    }

    return spath;
}

std::vector<int32_t> validAddrByInode(FileSystem *fileSystem, pseudo_inode &inode, bool all){
    std::fstream file;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    std::vector<int32_t> addresses;
    if (inode.direct1 > 0) addresses.push_back(inode.direct1);
    if (inode.direct2 > 0) addresses.push_back(inode.direct2);
    if (inode.direct3 > 0) addresses.push_back(inode.direct3);
    if (inode.direct4 > 0) addresses.push_back(inode.direct4);
    if (inode.direct5 > 0) addresses.push_back(inode.direct5);
    if (inode.indirect1 > 0) {

        if (all)
            addresses.push_back(inode.indirect1);

        int32_t links_count = fileSystem->super_block.cluster_size / sizeof(int32_t);
        int32_t links[links_count];
        file.seekg(inode.indirect1);
        file.read(reinterpret_cast<char *>(&links), sizeof(links));
        for (int i = 0; i < links_count; i++) {
            if (links[i] > 0) {
                addresses.push_back(links[i]);
            }
        }
        if (inode.indirect2 > 0) {

            if (all)
                addresses.push_back(inode.indirect2);

            int32_t secLinks[links_count];
            file.seekg(inode.indirect2);
            file.read(reinterpret_cast<char *>(&links), sizeof(links));
            for (int i = 0; i < links_count; i++) {
                if (links[i] > 0) {

                    if (all)
                        addresses.push_back(links[i]);

                    file.seekg(links[i]);
                    file.read(reinterpret_cast<char *>(&secLinks), sizeof(secLinks));
                    for (int j = 0; j < links_count; j++) {
                        if (secLinks[j]) addresses.push_back(secLinks[j]);
                    }
                }
            }
        }
    }
    file.close();
    return addresses;
}

int32_t findDirName(FileSystem *fileSystem, const std::vector<int32_t>& addresses, const std::string& dirName){
    std::fstream file;
    if (file.is_open())
        file.close();

    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t dirs_count = fileSystem->super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];

    for (auto &address: addresses) {
        file.seekg(address);
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id > 0 && dirs[i].item_name == dirName) {
                pseudo_inode inode{};
                file.seekg(getINodeLocation(fileSystem, dirs[i].inode_id));
                file.read(reinterpret_cast<char *>(&inode), sizeof(pseudo_inode));
                if (inode.isDirectory) {
                    file.close();
                    return inode.nodeid;
                }

            }
        }
    }

    file.close();
    return 0;
}

int32_t getFreeInodeIndex(FileSystem *fileSystem, int32_t lastFull) {
    std::fstream file;
    int32_t i = fileSystem->super_block.bitmapi_start_address + lastFull;
    uint8_t byte;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    // pokud nepůjde nextfit, bude firstfit
    if (lastFull >= fileSystem->super_block.cluster_count)
        i = fileSystem->super_block.bitmapi_start_address + 1;

    file.seekg(i);
    file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
    for (int j = 0; j < fileSystem->super_block.cluster_count; j++) {
        if (byte != 0) {
            if (i == fileSystem->super_block.cluster_count + fileSystem->super_block.bitmapi_start_address)
                i = fileSystem->super_block.bitmapi_start_address;
            i++;
            file.seekg(i);
            file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
        }
        else {
            i++;
            i -= fileSystem->super_block.bitmapi_start_address;
            return i;
        }
    }
    return 0;
}

int32_t getFreeDataIndex(FileSystem *fileSystem){
    std::fstream file;

    uint8_t byte;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t i = fileSystem->super_block.bitmap_start_address;

    file.seekg(i);
    file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
    for (int j = 0; j < fileSystem->super_block.cluster_count; j++) {
        if (byte != 0) {
            if (i == fileSystem->super_block.cluster_count)
                i = fileSystem->super_block.bitmap_start_address;
            i++;
            file.seekg(i);
            file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
        }
        else {
            return i - fileSystem->super_block.bitmap_start_address;
        }
    }
    return 0;
}

bool makeEmptyDir(FileSystem *fileSystem, int32_t freeInodeIndex, int32_t parentIndex, const std::string& name){
    int32_t freeBitmapIndex = getFreeDataIndex(fileSystem);
    int32_t freeDataAddr = (freeBitmapIndex * fileSystem->super_block.cluster_size) + fileSystem->super_block.data_start_address;
    if (freeDataAddr == 0) {
        std::cout << "NO FREE SPACE LEFT" << std::endl;
        return false;
    }

    pseudo_inode inode{};
    inode.nodeid = freeInodeIndex;
    inode.isDirectory = true;
    inode.references++;
    inode.direct1 = freeDataAddr;
    inode.direct2 = 0;
    inode.direct3 = 0;
    inode.direct4 = 0;
    inode.direct5 = 0;
    inode.indirect1 = 0;
    inode.indirect2 = 0;
    inode.file_size = fileSystem->super_block.cluster_size;

    directory_item toParent = createDirectoryItem(freeInodeIndex, name);
    if (!addDirectoryToParrent(fileSystem, toParent, parentIndex)) {
        std::cout << "PARENT DIRECTORY IS FULL" << std::endl;
        return false;
    }
    std::vector<directory_item> dir_vec;
    dir_vec.emplace_back(createDirectoryItem(inode.nodeid, "."));
    dir_vec.emplace_back(createDirectoryItem(parentIndex, ".."));
    for (int i = 2; i < (fileSystem->super_block.cluster_size / sizeof(directory_item)); i++)
        dir_vec.emplace_back(createDirectoryItem(0, ""));

    uint8_t one = 1;

    std::fstream input;
    if (input.is_open())
        input.close();

    // otevře soubor k zapsání fs
    input.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    // zapíše do bitmapy i-nodů
    input.seekg(freeInodeIndex - 1 + fileSystem->super_block.bitmapi_start_address);
    input.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));
    // zapíše do bitmapy dat
    input.seekg(freeBitmapIndex + fileSystem->super_block.bitmap_start_address);
    input.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));

    // zápis i-nodu
    input.seekg(getINodeLocation(fileSystem, freeInodeIndex));
    input.write(reinterpret_cast<const char *>(&inode), sizeof(pseudo_inode));

    // zápis dat
    input.seekg(freeDataAddr);
    for (auto &dir : dir_vec)
        input.write((const char *) &dir, sizeof(directory_item));
    input.flush();
    input.close();
    return true;
}

bool addDirectoryToParrent(FileSystem *fileSystem, directory_item &dir, int32_t parentIndex){
    pseudo_inode parent = loadInode(fileSystem, parentIndex);
    std::vector<int32_t> addresses = validAddrByInode(fileSystem, parent, false);
    int32_t null = 0;
    std::fstream input;
    if (input.is_open())
        input.close();

    // otevře soubor k zapsání fs
    input.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t dirs_count = fileSystem->super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];
    bool written = false;
    for (auto &address: addresses) {
        // načtení složek
        input.seekg(address);
        input.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id > null) {
                continue;
            }
            else {
                dirs[i] = dir;
                written = true;
                break;
            }

        }
        if (written) {
            input.seekg(address);
            input.write(reinterpret_cast<char *>(&dirs), sizeof(dirs));
            input.flush();
            input.close();
            return true;
        }
    }
    input.close();
    return false;
}

pseudo_inode loadInode(FileSystem *fileSystem, int32_t nodeId){
    std::fstream file;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);
    pseudo_inode inode{};
    file.seekg(getINodeLocation(fileSystem, nodeId));
    file.read(reinterpret_cast<char *>(&inode), sizeof(pseudo_inode));

    return inode;
}

void printFullBitIndexes(FileSystem *fileSystem, int32_t addr){
    std::fstream file;
    std::vector<int8_t> buff;
    int8_t byte;
    if (file.is_open())
        file.close();

    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);
    file.seekg(addr);
    for (int i = 1; i < 50; ++i) {
        file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
        file.seekg(addr + i);
        buff.emplace_back(byte);
    }
    file.close();

    for (auto i : buff) {
        if (i == 1)
            std::cout << 1 ;
        else std::cout << 0 ;
    }
    std::cout << std::endl;
}

bool isDirEmpty(FileSystem *fileSystem, pseudo_inode &inode, std::fstream &file){

    int32_t dirs_count = fileSystem->super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];
    std::vector<int32_t> addresses = validAddrByInode(fileSystem, inode, true);

    for (auto &address: addresses) {

        file.seekg(address);
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));


        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id > 0 && i < 2) continue; // přeskočí se "." a ".."
            if (dirs[i].inode_id > 0) return false;
        }
    }
    return true;
}

bool removeInode(FileSystem *fileSystem, std::fstream &file, pseudo_inode &inodeToRemove){
    inodeToRemove.references--;

    pseudo_inode inode{};
    inode.isDirectory = false;
    inode.nodeid = 0;
    inode.file_size = 0;
    inode.direct1 = 0;
    inode.direct2 = 0;
    inode.direct3 = 0;
    inode.direct4 = 0;
    inode.direct5 = 0;
    inode.indirect1 = 0;
    inode.indirect2 = 0;

    int8_t byte = 0;

    file.seekg(getINodeLocation(fileSystem, inodeToRemove.nodeid));
    if (inodeToRemove.references > 0) {
        file.write(reinterpret_cast<const char *>(&inodeToRemove), sizeof(pseudo_inode));
        file.flush();
        return false;
    }
    file.write(reinterpret_cast<const char *>(&inode), sizeof(pseudo_inode));
    // vymaže záznam bitmapě inodů
    file.seekg(inodeToRemove.nodeid - 1 + fileSystem->super_block.bitmapi_start_address);
    file.write(reinterpret_cast<const char *>(&byte), sizeof(byte));
    file.flush();
    return true;
}

void removeDirectoryItFromParrent(FileSystem *fileSystem, std::fstream &file, pseudo_inode &inode, pseudo_inode &parent){
    std::vector<int32_t> addresses = validAddrByInode(fileSystem, parent, false);

    int32_t dirs_count = fileSystem->super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];

    for (auto &address: addresses) {

        file.seekg(address);
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id == inode.nodeid) {
                dirs[i] = createDirectoryItem(0, "");
                file.seekg(address);
                file.write(reinterpret_cast<const char *>(&dirs), sizeof(dirs));
                file.flush();
                return;
            }
        }
    }
}

void removeDirectoryIt(FileSystem *fileSystem, std::fstream &file, pseudo_inode &parent, std::string &name){
    std::vector<int32_t> addresses = validAddrByInode(fileSystem, parent, false);

    int32_t dirs_count = fileSystem->super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];

    for (auto &address: addresses) {

        file.seekg(address);
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].item_name == name) {
                dirs[i] = createDirectoryItem(0, "");
                file.seekg(address);
                file.write(reinterpret_cast<const char *>(&dirs), sizeof(dirs));
                file.flush();
                return;
            }
        }
    }
}

void removeDataBlocks(FileSystem *fileSystem, std::fstream &file, pseudo_inode &inode){
    std::vector<int32_t> addresses = validAddrByInode(fileSystem, inode, true);
    int8_t zero = 0;

    for (auto & addr: addresses) {
        file.seekg(addr);
        for (int i = 0; i < fileSystem->super_block.cluster_size; i++) {
            file.write("\0", 1);
            file.flush();
        }
        int32_t  bitmapIndex = (addr - fileSystem->super_block.data_start_address) / fileSystem->super_block.cluster_size;
        file.seekg(fileSystem->super_block.bitmap_start_address + bitmapIndex);
        file.write(reinterpret_cast<const char *>(&zero), sizeof(zero));
        file.flush();
    }
}

int32_t findFileName(FileSystem *fileSystem, const std::vector<int32_t>& addresses, std::string& name){
    std::string fileName;
    std::string otherName;
    std::fstream file;
    if (file.is_open())
        file.close();

    fileName = name;
    // odstranění přípony
    if (name.size() > 4 && name[name.size() - 4] == '.') {
        fileName.erase(name.size() - 4, 4);
    }

    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t dirs_count = fileSystem->super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];

    for (auto &address: addresses) {
        file.seekg(address);
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id > 0) {
                otherName = dirs[i].item_name;
                if (otherName.size() > 4 && otherName[otherName.size() - 4] == '.')
                    otherName.erase(otherName.size() - 4, 4);
                if (fileName == otherName) {
                    pseudo_inode inode{};
                    file.seekg(getINodeLocation(fileSystem, dirs[i].inode_id));
                    file.read(reinterpret_cast<char *>(&inode), sizeof(pseudo_inode));
                    file.close();
                    return inode.nodeid;
                }
            }
        }
    }

    file.close();
    return 0;
}

int32_t getFreeDataCount(FileSystem *fileSystem) {
    std::fstream file;
    int32_t counter = 0;
    uint8_t byte;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t i = fileSystem->super_block.bitmap_start_address;

    file.seekg(i);
    file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
    for (int j = 0; j < fileSystem->super_block.cluster_count; j++) {
        if (byte == 0) {
            counter++;
        }
        i++;
        file.seekg(i);
        file.read(reinterpret_cast<char *>(&byte), sizeof(byte));
    }
    return counter;
}

void writeData(FileSystem *fileSystem, const std::string& source, std::fstream &file, int32_t freeInodeI, double srcSize){
    pseudo_inode inode{};

    inode.nodeid = freeInodeI;
    inode.isDirectory = false;
    inode.references++;
    inode.file_size = srcSize;

    int32_t links_count = fileSystem->super_block.cluster_size / sizeof(int32_t);
    int32_t links[links_count];

    int8_t stop = 0;
    std::ifstream src;
    src.open(source, std::ios::binary | std::ios::in);
    inode.direct1 = writeToDatablock(fileSystem,  src, file, stop);
    if (stop == 0) {
        inode.direct2 = writeToDatablock(fileSystem, src, file, stop);
    }
    if (stop == 0) {
        inode.direct3 = writeToDatablock(fileSystem, src, file, stop);
    }
    if (stop == 0) {
        inode.direct4 = writeToDatablock(fileSystem, src, file, stop);
    }
    if (stop == 0) {
        inode.direct5 = writeToDatablock(fileSystem, src, file, stop);
    }
    if (stop == 0) {
        inode.indirect1 = createIndirectDatab(fileSystem, file);

        for (int i = 0; i < links_count; i++) {
            links[i] = writeToDatablock(fileSystem, src, file, stop);
            file.seekg(inode.indirect1 + i * sizeof(int32_t));
            file.write(reinterpret_cast<const char *>(&links[i]), sizeof(int32_t));

            if (stop == 1)
                break;
        }
    }
    if (stop == 0) {
        inode.indirect2 = createIndirectDatab(fileSystem, file);
        file.seekg(inode.indirect2);
        file.read(reinterpret_cast<char *>(&links), sizeof(links));
        for (auto &link: links) {
            link = createIndirectDatab(fileSystem, file);

            // zápis do 1. vrtvy odkazu 2. řádu
            file.seekg(inode.indirect2);
            file.write(reinterpret_cast<char *>(&links), fileSystem->super_block.cluster_size);

            int32_t subLinks[links_count];
            file.seekg(link);
            file.read(reinterpret_cast<char *>(&subLinks), sizeof(subLinks));
            for (int i = 0; i < links_count; i++) {
                subLinks[i] = writeToDatablock(fileSystem, src, file, stop);
                file.seekg(link + i * sizeof(int32_t));
                file.write(reinterpret_cast<const char *>(&subLinks[i]), sizeof(int32_t));
                if (stop == 1)
                    break;

            }
            if (stop == 1)
                break;


        }

    }
    uint8_t one = 1;
    file.seekg(freeInodeI - 1 + fileSystem->super_block.bitmapi_start_address);
    file.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));

    file.seekg(getINodeLocation(fileSystem, freeInodeI));
    file.write(reinterpret_cast<const char *>(&inode), sizeof(inode));

}

int32_t writeToDatablock(FileSystem *fileSystem,  std::ifstream &src, std::fstream &file, int8_t &stop){
    int32_t dataBlockIndex = getFreeDataIndex(fileSystem);

    int32_t dataBAddres = dataBlockIndex * fileSystem->super_block.cluster_size + fileSystem->super_block.data_start_address;


    std::vector<char> buff;
    buff.resize(fileSystem->super_block.cluster_size);
    src.read(buff.data(), fileSystem->super_block.cluster_size);
    file.seekg(dataBAddres);
    file.write(buff.data(), fileSystem->super_block.cluster_size);

    // obsadí index v bitmapě
    uint8_t one = 1;
    file.seekg(fileSystem->super_block.bitmap_start_address + dataBlockIndex);
    file.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));
    file.flush();

    if (src.eof())
        stop = 1;
    return dataBAddres;
}

int32_t createIndirectDatab(FileSystem *fileSystem, std::fstream &file){
    int32_t links_count = fileSystem->super_block.cluster_size / sizeof(int32_t);
    int32_t links[links_count];
    for (auto &link: links) {
        link = 0;
    }
    int32_t  freeDataIndex = getFreeDataIndex(fileSystem);

    file.seekg(freeDataIndex * fileSystem->super_block.cluster_size + fileSystem->super_block.data_start_address);
    file.write(reinterpret_cast<const char *>(&links), fileSystem->super_block.cluster_size);
    //obsadí index v bitmapě
    int8_t one = 1;
    file.seekg(freeDataIndex + fileSystem->super_block.bitmap_start_address);
    file.write(reinterpret_cast<const char *>(&one), sizeof(int8_t));
    file.flush();

    return freeDataIndex * fileSystem->super_block.cluster_size + fileSystem->super_block.data_start_address;

}

void copyData(FileSystem *fileSystem, const std::vector<int32_t>& addressesToCopy, pseudo_inode &newInode){
    int32_t links_count = fileSystem->super_block.cluster_size / sizeof(int32_t);
    int32_t links[links_count];

    int iterator = 0;
    std::fstream file;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);
    newInode.direct1 = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
    iterator++;
    if (iterator < addressesToCopy.size()) {
        newInode.direct2 = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
        iterator++;
    }
    if (iterator < addressesToCopy.size()) {
        newInode.direct3 = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
        iterator++;
    }
    if (iterator < addressesToCopy.size()) {
        newInode.direct4 = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
        iterator++;
    }
    if (iterator < addressesToCopy.size()) {
        newInode.direct5 = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
        iterator++;
    }
    if (iterator < addressesToCopy.size()) {
        newInode.indirect1 = createIndirectDatab(fileSystem, file);

        for (int i = 0; i < links_count; i++) {
            links[i] = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
            iterator++;
            file.seekg(newInode.indirect1 + i * sizeof(int32_t));
            file.write(reinterpret_cast<const char *>(&links[i]), sizeof(int32_t));

            if (iterator >= addressesToCopy.size())
                break;
        }

    }
    if (iterator < addressesToCopy.size()) {
        newInode.indirect2 = createIndirectDatab(fileSystem, file);

        file.seekg(newInode.indirect2);
        file.read(reinterpret_cast<char *>(&links), sizeof(links));
        for (auto &link: links) {
            link = createIndirectDatab(fileSystem, file);

            // zápis do 1. vrtvy odkazu 2. řádu
            file.seekg(newInode.indirect2);
            file.write(reinterpret_cast<char *>(&links), fileSystem->super_block.cluster_size);

            int32_t subLinks[links_count];
            file.seekg(link);
            file.read(reinterpret_cast<char *>(&subLinks), sizeof(subLinks));
            for (int i = 0; i < links_count; i++) {
                subLinks[i] = copyDataBlock(fileSystem, addressesToCopy[iterator], file);
                iterator++;
                file.seekg(link + i * sizeof(int32_t));
                file.write(reinterpret_cast<const char *>(&subLinks[i]), sizeof(int32_t));
                if (iterator >= addressesToCopy.size())
                    break;

            }
            if (iterator >= addressesToCopy.size())
                break;


        }

    }
    uint8_t one = 1;
    file.seekg(newInode.nodeid - 1 + fileSystem->super_block.bitmapi_start_address);
    file.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));

    file.seekg(getINodeLocation(fileSystem, newInode.nodeid));
    file.write(reinterpret_cast<const char *>(&newInode), sizeof(newInode));


    file.close();
}

int32_t copyDataBlock(FileSystem *fileSystem, int32_t addrToCopy, std::fstream &file){
    int32_t dataBlockIndex = getFreeDataIndex(fileSystem);

    int32_t dataBAddres = dataBlockIndex * fileSystem->super_block.cluster_size + fileSystem->super_block.data_start_address;


    std::vector<char> buff;
    buff.resize(fileSystem->super_block.cluster_size);

    file.seekg(addrToCopy);
    file.read(buff.data(), fileSystem->super_block.cluster_size);

    file.seekg(dataBAddres);
    file.write(buff.data(), fileSystem->super_block.cluster_size);

    // obsadí index v bitmapě
    uint8_t one = 1;
    file.seekg(fileSystem->super_block.bitmap_start_address + dataBlockIndex);
    file.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));
    file.flush();

    return dataBAddres;
}

void removeDirectoryItFromParrent(FileSystem *fileSystem,  pseudo_inode &inode, pseudo_inode &parent){
    std::fstream file;
    if (file.is_open())
        file.close();
    file.open(fileSystem->fs_path, std::ios::binary | std::ios::in | std::ios::out);
    removeDirectoryItFromParrent(fileSystem, file, inode, parent);
    file.close();
}