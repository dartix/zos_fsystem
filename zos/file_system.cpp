//
// Created by daniel on 11.12.2020.
//



#include <cmath>
#include "file_system.h"
#include "utils.h"

FileSystem::FileSystem(std::string path, bool info) {

    fs_path = std::move(path);
    std::fstream file;

    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

    if (!file.is_open()) {
        std::cout << "CANNOT OPEN FILE WITH i-FS" << std::endl;
        return;
    } else std::cout << "FILE WITH i-FS OPENED" << std::endl;

    file.read(reinterpret_cast<char *>(&super_block), sizeof(superblock));
    if (info) {
        std::cout << "i-FS description:" << std::endl
                  << super_block.signature << std::endl << "disk_size " << super_block.disk_size << std::endl <<
                  "cluster_size " << super_block.cluster_size << std::endl << "cluster_count "
                  << super_block.cluster_count << std::endl <<
                  "bitmap_start_address " << super_block.bitmap_start_address << std::endl << "bitmapi_start_address "
                  <<
                  super_block.bitmapi_start_address << std::endl <<
                  "data_start " << super_block.data_start_address << std::endl << "inodes_start "
                  << super_block.inode_start_address << std::endl;
    }
    file.seekg(super_block.inode_start_address);
    file.read(reinterpret_cast<char *>(&root), sizeof(pseudo_inode));

    current = root;


    file.close();

}

FileSystem::FileSystem(int32_t size, std::string path) {

    fs_path = std::move(path);
    std::fstream input;

    if (input.is_open())
        input.close();


    // otevře soubor k zapsání fs
    input.open(fs_path, std::ios::binary | std::ios::trunc | std::ios::in | std::ios::out);

    // zapíše nuly
    for (int i = 0; i < size; i++)
        input.write("\0", 1);

    // instance superbloku
    std::make_unique<superblock>(super_block);

    // init superbloku
    std::string sign = "dschnurp";
    std::copy(sign.begin(), sign.end() + '\0', super_block.signature);
    std::string description = "popis...";
    std::copy(description.begin(), description.end() + '\0', super_block.volume_descriptor);



    // 103MB
    if (size > 104857600) {
        super_block.cluster_size = 4096;
    }
    // menší
    else {
        super_block.cluster_size = 1024;
    }

    int32_t remainings = size - sizeof(superblock);

    int inode_counter = remainings / super_block.cluster_size;

    remainings -= inode_counter * sizeof(pseudo_inode);

    int bitmap_size = 0;
    do {

        if (bitmap_size % 8 == 0) {
            remainings--;
        }
        bitmap_size++;
        remainings -= super_block.cluster_size;
    } while (remainings > 0);

    // zarovnání na 1 Byte
    int crop = bitmap_size % 8;
    bitmap_size -= crop;


    // disková velikost je tedy složena ze \/ superbloku, \/ bitmapy, a datových bloků \/
    super_block.disk_size = sizeof(superblock) + bitmap_size + inode_counter + super_block.cluster_size * bitmap_size;
    // počet klastrů je vlastně velikost bitmapy
    super_block.cluster_count = bitmap_size;

    super_block.bitmapi_start_address = sizeof(super_block);
    super_block.bitmap_start_address = super_block.bitmapi_start_address + bitmap_size;
    super_block.inode_start_address = super_block.bitmap_start_address + bitmap_size;
    super_block.data_start_address = super_block.inode_start_address + sizeof(pseudo_inode) * inode_counter;

    // zápis strukrur do souboru
    input.seekg(0, std::ios::beg);
    input.write(reinterpret_cast<const char *>(&super_block), sizeof(superblock));

    input.flush();

    // ////////////////////////////////////////////////////////////////////////////////
    // tvorba "/" kořenového adresáře
    pseudo_inode inode{};
    inode.nodeid = 1;
    inode.isDirectory = true;
    inode.references++;
    inode.direct1 = super_block.data_start_address;
    inode.direct2 = 0;
    inode.direct3 = 0;
    inode.direct4 = 0;
    inode.direct5 = 0;
    inode.indirect1 = 0;
    inode.indirect2 = 0;
    inode.file_size = super_block.cluster_size;



    std::vector<directory_item> dir_vec;
    dir_vec.emplace_back(createDirectoryItem(inode.nodeid, "."));
    dir_vec.emplace_back(createDirectoryItem(inode.nodeid, ".."));
    for (int i = 2; i < (super_block.cluster_size / sizeof(directory_item)); i++)
        dir_vec.emplace_back(createDirectoryItem(0, ""));

    root = inode;
    current = inode;
    uint8_t one = 1;

    // zapíše do bitmapy i-nodů
    input.seekg(super_block.bitmapi_start_address);
    input.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));

    // zapíše do bitmapy dat
    input.seekg(super_block.bitmap_start_address);
    input.write(reinterpret_cast<const char *>(&one), sizeof(uint8_t));

    // zápis i-nodu
    input.seekg(super_block.inode_start_address);
    input.write(reinterpret_cast<const char *>(&inode), sizeof(inode));

    // zápis dat
    input.seekg(super_block.data_start_address);
    for (auto &dir : dir_vec)
        input.write((const char *) &dir, sizeof(directory_item));

    input.close();

    std::cout << "OK" << std::endl;


}




pseudo_inode FileSystem::cd(const std::string& path, bool stay) {
    pseudo_inode inode{};
    std::vector<int32_t> addresses;
    int32_t destInode;

    std::vector<std::string> spath = parsePath(path);
    if (spath[0] == "NULL!") {
        std::cout << "PATH NOT FOUND" << std::endl;
        return inode;
    } // "cd /"
    else if (spath[0].empty() && spath.size() == 1 && stay) {
        current = root;
        std::cout << "OK" << std::endl;
        return inode;
    }
    else if (spath[0].empty() && spath.size() == 1)
        return root;
 // relativní cesta
    else if (!spath[0].empty() && spath.size() == 1) {
        addresses = validAddrByInode(this, current, false);
    }
    else {
        addresses = validAddrByInode(this, root, false);
        spath.erase(spath.begin() + 0);
    }



    for (int i = 0; i < spath.size(); i++) {
        if (i == spath.size() - 1)
            destInode = findFileName(this, addresses, spath[i]);
        else destInode = findDirName(this, addresses, spath[i]);
        if (destInode == 0) {
            inode.nodeid = 0;
            return inode;
        }
        std::fstream file;
        if (file.is_open())
            file.close();

        file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

        // skočí na adresu nového aktuálního inodu
        int32_t addr = getINodeLocation(this, destInode);
        file.seekg(addr);

        file.read(reinterpret_cast<char *>(&inode), sizeof(pseudo_inode));
        file.close();
        addresses = validAddrByInode(this, inode, false);
    }

    if (stay && !inode.isDirectory) {
        std::cout << "PATH NOT FOUND (THIS IS NOT DIRECTORY)" << std::endl;
        return inode;
    }


    if (stay) {
        if (destInode == 0) {
            std::cout << "PATH NOT FOUND" << std::endl;
            return inode;
        }
        current = inode;
        std::cout << "OK" << std::endl;
    }
    return inode;
}


std::string FileSystem::pwd() {
    std::stack<std::string> spath;
    std::string path = "/";
    std::fstream file;
    int32_t dirs_count = super_block.cluster_size / sizeof(directory_item) - 2;
    directory_item dirs[dirs_count]; // přeskočí se "." a ".."

    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

    directory_item parent{};
    pseudo_inode node = current;
    pseudo_inode previous = current;
    while (node.nodeid != root.nodeid) {
        // skočí se na 2. dir item, což je rodičovský inode (adresa)... proto + dir_item
        file.seekg(node.direct1 + sizeof(directory_item));
        file.read(reinterpret_cast<char *>(&parent), sizeof(parent));
        // inode rodiče
        file.seekg(getINodeLocation(this, parent.inode_id));
        file.read(reinterpret_cast<char *>(&node), sizeof(node));
        // jméno rodičovského adresáře... přeskočí se "." a ".."
        file.seekg(node.direct1 + (2 * sizeof(directory_item)));
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id == previous.nodeid) {
                spath.push(dirs[i].item_name);
                previous = node;
                break;
            }
        }
    }

    while (!spath.empty()) {
        path.append(spath.top());
        path.append("/");
        spath.pop();
    }
    file.close();
    return path;
}

void FileSystem::mkdir(const std::string& name) {

    std::vector<std::string> spath = parsePath(name);
    std::string dirDest;
    std::vector<int32_t> addresses;
    int32_t destInode;
    pseudo_inode inode{};
    if (spath[0] == "NULL!") {
        std::cout << "EMPTY NAME" << std::endl;
        return;
    }

    if (spath[spath.size() - 1].size() > 5 &&
    spath[spath.size() - 1].substr(spath[spath.size() - 1].size() - 4, 1) == ".") {
        std::cout << "INVALID NAME" << std::endl;
        return;
    }


    if (!spath[0].empty() && spath.size() == 1) {
        addresses = validAddrByInode(this, current, false);
        inode = current;
    }
    else {
        for (int i = 0; i < spath.size() - 1; i++) {
            if (!spath[i].empty())
                dirDest += "/" + spath[i];
        }
        if (dirDest.empty())
            dirDest = "/";
        inode = cd(dirDest, false);
        addresses = validAddrByInode(this, inode, false);
    }

    destInode = findFileName(this, addresses, spath[spath.size() - 1]);
    if ( destInode > 0) {
            std::cout << "EXIST" << std::endl;
            return;
        }

    int32_t freeInodeIndex = getFreeInodeIndex(this, inode.nodeid);

    if (freeInodeIndex == 0) {
        std::cout << "NO FREE SPACE LEFT" << std::endl;
        return;
    }
    if (makeEmptyDir(this, freeInodeIndex, inode.nodeid, spath[spath.size() -1]))
        std::cout << "OK" << std::endl;
}

void FileSystem::ls() {
    // obsazené databloky daného adresáře
    std::vector<int32_t> addresses;

        addresses = validAddrByInode(this, current, false);


    std::fstream file;
    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t dirs_count = super_block.cluster_size / sizeof(directory_item);
    directory_item dirs[dirs_count];

    for (auto &address: addresses) {
        file.seekg(address);
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id > 0) {
                pseudo_inode inode{};
                file.seekg(getINodeLocation(this, dirs[i].inode_id));
                file.read(reinterpret_cast<char *>(&inode), sizeof(pseudo_inode));
                if (inode.isDirectory) {
                    std::cout << "+ ";
                }
                else {
                    std::cout << "- ";
                }
                std::cout << "\t" << dirs[i].inode_id << "\t"
                          << inode.file_size<< "\t" << dirs[i].item_name << std::endl;
            }
        }
    }

    file.close();
}

void FileSystem::rmdir(const std::string &path) {
    pseudo_inode inode = cd(path, false);
    if (inode.nodeid == 0)
        return;
    if (inode.nodeid == current.nodeid) {
        std::cout << "cd /" << std::endl;
        cd("/", true);
        std::cout << "rmdir..." << std::endl;
    }

    if (inode.isDirectory && inode.nodeid > 0) {

        std::vector<int32_t> addresses = validAddrByInode(this, inode, false);
        int32_t parentId = findDirName(this, addresses, "..");
        pseudo_inode parent = loadInode(this, parentId);


        if (parentId == inode.nodeid) {
            std::cout << "FORBIDDEN, THIS IS ROOT"<< std::endl;
            return;
        }

        std::fstream file;
        if (file.is_open())
            file.close();

        file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

        if (!isDirEmpty(this, inode, file)) {
            file.close();
            std::cout << "NOT EMPTY" << std::endl;
            return;
        }

        removeDirectoryItFromParrent(this, file, inode, parent);

        if (inode.references <= 1)
            removeDataBlocks(this, file, inode);

        if (!removeInode(this, file, inode)) {
            file.close();
            std::cout << "FILE HAS HARDLINK... REFERENCE DECREASED"<< std::endl;
            return;
        }



        file.close();
        std::cout << "OK" << std::endl;
    }
    else {
        std::cout << "FILE NOT FOUND" << std::endl;
    }
}

void FileSystem::info(const std::string &path) {
    pseudo_inode inode = cd(path, false);
    if (inode.nodeid == 0)
        return;
    std::vector<int32_t> addresses = validAddrByInode(this, inode, true);
    std::vector<std::string> spath = parsePath(path);
    int32_t destInode = 0;
    if (inode.nodeid > 0) {
        if (spath.size() == 1) {
            destInode = current.nodeid;
        }
        else if (spath[0].empty() && spath.size() == 2) {
            destInode = root.nodeid;
        }
        else if (spath.size() > 1) {
            if (inode.isDirectory) {
                destInode = findDirName(this, addresses, spath[spath.size() - 2]);
            } else {
                destInode = inode.nodeid;
            }
        }
        if (destInode == 0) {
            std::cout << "FILE NOT FOUND" << std::endl;
            return;
        }
        std::cout << "NAME: \t\t" << spath[spath.size() - 1] << std::endl;
        std::cout << "REFS: \t\t" << std::to_string(inode.references) << std::endl;
        std::cout << "SIZE: \t\t" << inode.file_size << std::endl;
        std::cout << "INODE_ID: \t\t" << inode.nodeid << std::endl;
        std::cout << "DIRECTORY: \t\t" << inode.isDirectory << std::endl;
        std::cout << "FULL_ADDRESSES: \t\t";
        for (auto &address : addresses) {
            std::cout << address << " ";
        }
        std::cout << std::endl;
        std::cout << "indirect 1 " << inode.indirect1 << " indirect 2 " << inode.indirect2 <<std::endl;
    }
    else {
        std::cout << "FILE NOT FOUND" << std::endl;
    }

}

void FileSystem::incp(const std::string &source, const std::string &dest) {
    std::ifstream src;
    std::string dirDest;

    src.open(source, std::ios::ate | std::ifstream::binary);
    double srcSize = src.tellg();
    src.close();
    if (srcSize == -1) {
        std::cout << "PATH TO SOURCE NOT FOUND" << std::endl;
        return;
    }

    int32_t freeInodeI = getFreeInodeIndex(this, 0);
    if (freeInodeI == 0) {
        std::cout << "ALL INODES ARE FULL" << std::endl;
        return;
    }

    std::vector<std::string> spath = parsePath(dest);
    for (int i = 0; i < spath.size() - 1; i++) {
        if (!spath[i].empty())
            dirDest += "/" + spath[i];
    }
    if (dirDest.empty())
        dirDest = "/";

    pseudo_inode inode = cd(dirDest, false);
    if (inode.isDirectory && inode.nodeid > 0) {

        std::vector<int32_t> addresses = validAddrByInode(this, inode, false);
        if (findFileName(this, addresses, spath[spath.size() - 1]) > 0) {
            std::cout << "IN THIS DESTINATION THE NAME ALREADY EXIST" << std::endl;
            return;
        }


        int32_t datablockNeeded = std::ceil((srcSize) / (double) (super_block.cluster_size));
        int32_t links_count = super_block.cluster_size / sizeof(int32_t);
        int32_t freeDatablockCount = getFreeDataCount(this);
        if (datablockNeeded > 5) {
            datablockNeeded += ceil((double) datablockNeeded / (double) links_count) + 1;
        }
        if (datablockNeeded > freeDatablockCount) {
            std::cout << "NOT ENOUGH SPACE" << std::endl;
            return;
        }

        // vytvoření položky v nadřazeném adresáři
        directory_item directoryItem = createDirectoryItem(freeInodeI, spath[spath.size() - 1]);
        std::fstream file;
        if (file.is_open())
            file.close();
        file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);
        file.seekg(inode.direct1);
        int32_t dirs_count = super_block.cluster_size / sizeof(directory_item);
        directory_item dirs[dirs_count];
        file.read(reinterpret_cast<char *>(&dirs), sizeof(dirs));
        for (int i = 0; i < dirs_count; i++) {
            if (dirs[i].inode_id == 0) {
                dirs[i] = directoryItem;
                file.seekg(inode.direct1);
                file.write(reinterpret_cast<char *>(&dirs), sizeof(dirs));
                file.flush();
                break;
            }
        }

        writeData(this, source, file, freeInodeI, srcSize);

        file.close();
        std::cout << "OK" << std::endl;
    }
    else {
        std::cout << "PATH TO DESTINATION NOT FOUND" << std::endl;
    }

}


void FileSystem::cat(const std::string &path) {

    pseudo_inode inode = cd(path, false);
    if (inode.isDirectory) {
        std::cout << "FILE NOT FOUND THIS IS DIRECTORY" << std::endl;
        return;
    }
    if (inode.nodeid == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }

    std::vector<int32_t> addresses = validAddrByInode(this, inode, false);

    std::fstream file;
    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

    std::vector<char> buff;
    int32_t fileSize = inode.file_size;
    int32_t size;
    buff.resize(super_block.cluster_size);
    for (auto &address: addresses) {
        fileSize -= super_block.cluster_size;
        if (fileSize < 0)
            size = fileSize + super_block.cluster_size;
        else size = super_block.cluster_size;
        file.seekg(address);
        file.read(buff.data(), size);
        std::cout << buff.data();
        buff.clear();
        buff.resize(size);
    }
    std::cout << std::endl;
    file.close();
}


void FileSystem::outcp(const std::string &source, const std::string &dest) {
    pseudo_inode inode = cd(source, false);
    if (inode.isDirectory) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }
    if (inode.nodeid == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }

    std::fstream output;
    output.open(dest, std::ios::binary | std::ios::trunc | std::ios::in | std::ios::out);
    if (!output.is_open()) {
        std::cout << "PATH NOT FOUND" << std::endl;
        return;
    }

    std::vector<int32_t> addresses = validAddrByInode(this, inode, false);

    std::fstream file;
    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

    int32_t fileSize = inode.file_size;
    int32_t size;
    std::vector<char> buff;
    buff.resize(super_block.cluster_size);
    for (int address: addresses) {
        fileSize -= super_block.cluster_size;
        if (fileSize < 0)
            size = fileSize + super_block.cluster_size;
        else size = super_block.cluster_size;
        file.seekg(address);
        file.read(buff.data(), size);
        output.write(buff.data(), size);
        buff.clear();

    }
    output.flush();
    output.close();
    file.close();
    std::cout << "OK" << std::endl;
}


void FileSystem::rm(const std::string &path) {
    pseudo_inode inode = cd(path, false);
    if (inode.nodeid == 0)
        return;
    std::string dirDest;
    std::vector<std::string> spath = parsePath(path);
    for (int i = 0; i < spath.size() - 1; i++) {
        if (!spath[i].empty())
            dirDest += "/" + spath[i];
    }
    if (dirDest.empty() && spath.size() == 1)
        dirDest = ".";
    else if (dirDest.empty())
        dirDest = "/";
    pseudo_inode parent = cd(dirDest, false);
    if (parent.nodeid == 0)
        return;
    if (inode.isDirectory && inode.nodeid == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }

    std::fstream file;
    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);
    // předěláno
    removeDirectoryIt(this, file, parent, spath[spath.size() - 1]);

    if (inode.references == 1)
        removeDataBlocks(this, file, inode);

    if (!removeInode(this, file, inode)) {
        file.close();
        std::cout << "FILE HAS HARDLINK... REFERENCE DECREASED"<< std::endl;
        return;
    }



    file.close();
    std::cout << "OK" << std::endl;



}

void FileSystem::ln(const std::string &old, const std::string &newLink) {
    pseudo_inode oldInode = cd(old, false);
    if (oldInode.nodeid == 0)
        return;
    std::vector<std::string> spath = parsePath(newLink);
    std::string dirDest;
    for (int i = 0; i < spath.size() - 1; i++) {
        if (!spath[i].empty())
            dirDest += "/" + spath[i];
    }
    if (dirDest.empty() && spath.size() == 1)
        dirDest = ".";
    else if (dirDest.empty())
        dirDest = "/";
    pseudo_inode parent = cd(dirDest, false);
    if (parent.nodeid == 0)
        return;
    std::vector<int32_t> addresses = validAddrByInode(this, parent, false);
    if (findFileName(this, addresses, spath[spath.size() - 1]) > 0) {
        std::cout << "IN THIS DESTINATION THE NAME ALREADY EXIST" << std::endl;
        return;
    }
    directory_item dir = createDirectoryItem(oldInode.nodeid, spath[spath.size() - 1]);
    if (!addDirectoryToParrent(this, dir, parent.nodeid)) {
        std::cout << "PARENT DIRECTORY IS FULL" << std::endl;
        return;
    }
    oldInode.references++;
    int32_t addr = getINodeLocation(this, oldInode.nodeid);
    std::fstream file;
    if (file.is_open())
        file.close();
    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);
    file.seekg(addr);
    file.write(reinterpret_cast<const char *>(&oldInode), sizeof(oldInode));
    file.flush();
    file.close();

    std::cout << "OK" << std::endl;

}

void FileSystem::cp(const std::string &source, const std::string &dest) {

    pseudo_inode oldInode = cd(source, false);
    if (oldInode.nodeid == 0)
        return;

    std::string dirDest;
    std::vector<std::string> spath = parsePath(dest);
    for (int i = 0; i < spath.size() - 1; i++) {
        if (!spath[i].empty())
            dirDest += "/" + spath[i];
    }
    if (dirDest.empty() && spath.size() == 1)
        dirDest = ".";
    else if (dirDest.empty())
        dirDest = "/";

    pseudo_inode parent = cd(dirDest, false);
    if (parent.nodeid == 0)
        return;
    std::vector<int32_t> addresses = validAddrByInode(this, parent, false);
    if (findFileName(this, addresses, spath[spath.size() - 1]) > 0) {
        std::cout << "IN THIS DESTINATION THE NAME ALREADY EXIST" << std::endl;
        return;
    }

    int32_t freeInodeIndex = getFreeInodeIndex(this, 0);
    if (freeInodeIndex == 0) {
        std::cout << "ALL INODES ARE FULL" << std::endl;
        return;
    }

    addresses.clear();
    addresses = validAddrByInode(this, oldInode, true);
    int32_t blocksNeeded = addresses.size();
    int32_t freeBlocs = getFreeDataCount(this);
    if (freeBlocs < blocksNeeded) {
        std::cout << "NOT ENOUGH FREE SPACE" << std::endl;
        return;
    }

    pseudo_inode newInode{};
    newInode.nodeid = freeInodeIndex;
    newInode.references = 1;
    newInode.isDirectory = false;
    newInode.file_size = oldInode.file_size;

    directory_item dir = createDirectoryItem(newInode.nodeid, spath[spath.size() - 1]);
    addresses.clear();
    addresses = validAddrByInode(this, oldInode, false);
    copyData(this, addresses, newInode);
    addDirectoryToParrent(this, dir, parent.nodeid);
    std::cout << "OK" << std::endl;

}

void FileSystem::mv(const std::string &source, const std::string &dest) {
    pseudo_inode oldInode = cd(source, false);
    if (oldInode.nodeid == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }


    std::string dirDest;
    std::vector<std::string> spath = parsePath(dest);
    for (int i = 0; i < spath.size() - 1; i++) {
        if (!spath[i].empty())
            dirDest += "/" + spath[i];
    }
    if (dirDest.empty() && spath.size() == 1)
        dirDest = ".";
    else if (dirDest.empty())
        dirDest = "/";

    pseudo_inode parent = cd(dirDest, false);
    if (parent.nodeid == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }

    std::string oldDirDest;
    std::vector<std::string> oldSpath = parsePath(source);
    for (int i = 0; i < oldSpath.size() - 1; i++) {
        if (!oldSpath[i].empty())
            oldDirDest += "/" + oldSpath[i];
    }
    if (oldDirDest.empty() && oldSpath.size() == 1)
        oldDirDest = ".";
    else if (oldDirDest.empty())
        oldDirDest = "/";

    pseudo_inode oldParent = cd(oldDirDest, false);
    if (parent.nodeid == 0) {
        std::cout << "FILE NOT FOUND" << std::endl;
        return;
    }


    pseudo_inode newDir = cd(dest, false);


    // přejmenování
    if (parent.nodeid == oldParent.nodeid && oldDirDest == dirDest &&
            spath[spath.size() - 1] != oldSpath[oldSpath.size() - 1] && !newDir.isDirectory) {

        removeDirectoryItFromParrent(this, oldInode, oldParent);
        directory_item dir = createDirectoryItem(oldInode.nodeid, spath[spath.size() - 1]);
        addDirectoryToParrent(this, dir, oldParent.nodeid);
    }
    // přemístění
    else {
        if (newDir.isDirectory && newDir.nodeid > 0) {
            removeDirectoryItFromParrent(this, oldInode, oldParent);
            directory_item dir = createDirectoryItem(oldInode.nodeid, oldSpath[oldSpath.size() - 1]);
            addDirectoryToParrent(this, dir, newDir.nodeid);
        }
        else {
            std::cout << "DESTINATION PATH  IS NOT DIRECTORY" << std::endl;
            return;
        }

    }


    std::cout << "OK" << std::endl;
}

void FileSystem::printFullBitIndexes() {

    std::fstream file;

    if (file.is_open())
        file.close();

    file.open(fs_path, std::ios::binary | std::ios::in | std::ios::out);

    if (!file.is_open()) {
        std::cout << "CANNOT OPEN FILE WITH i-FS" << std::endl;
        return;
    }

    file.read(reinterpret_cast<char *>(&super_block), sizeof(superblock));

    std::cout << "i-FS description:" << std::endl
              << super_block.signature << std::endl << "disk_size " << super_block.disk_size << std::endl <<
              "cluster_size " << super_block.cluster_size << std::endl << "cluster_count "
              << super_block.cluster_count << std::endl <<
              "bitmap_start_address " << super_block.bitmap_start_address << std::endl << "bitmapi_start_address "
              <<
              super_block.bitmapi_start_address << std::endl <<
              "data_start " << super_block.data_start_address << std::endl << "inodes_start "
              << super_block.inode_start_address << std::endl;


    std::cout << std::endl << "inody" << std::endl;
    ::printFullBitIndexes(this, super_block.bitmapi_start_address);
    std::cout << "databloky" << std::endl;
    ::printFullBitIndexes(this, super_block.bitmap_start_address);
}
