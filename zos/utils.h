//
// Created by daniel on 07.01.2021.
//

#ifndef ZOS_FS_UTILS_H
#define ZOS_FS_UTILS_H

#include "file_system.h"

/**
 * vytvoří položku v adresáři
 * @param node_id id inodu
 * @param item_name jméno
 * @return položka
 */
directory_item createDirectoryItem(int32_t node_id, std::string item_name);

/**
 * getr
 * @param fileSystem instance fs
 * @param nodeId id nodu
 * @return pozice inodu ve FS
 */
int32_t getINodeLocation(FileSystem *fileSystem, int32_t nodeId) ;

/**
 * zkontroluje existenci zadané cesty
 * @param fileSystem instance fs
 * @param path cesta
 * @return cesta rozparsovaná
 */
std::vector<std::string> parsePath(const std::string& path);

/**
 * najde platné adresy dat z inodu
 * @param fileSystem instance fs
 * @param inode inode
 * @param all zahrnout i adresy vlastní adresy nepřímích odkazů?
 * @return adresy
 */
std::vector<int32_t> validAddrByInode(FileSystem *fileSystem, pseudo_inode &inode, bool all);

/**
 * najde jméno daného adresáře?
 * @param fileSystem instance fs
 * @param addresses dodané adresy na disku
 * @param dirName adresář
 * @return 0 pokud nenajde, pokud najde tak číslo inode
 */
int32_t findDirName(FileSystem *fileSystem, const std::vector<int32_t>& addresses, const std::string& dirName);

/**
 * getter
 * @param fileSystem instance fs
 * @param lastFull poslední obsazený index
 * @return vrátí index volného inodu nebo nulu
 */
int32_t getFreeInodeIndex(FileSystem *fileSystem, int32_t lastFull) ;

/**
 * getter
 * @param fileSystem instance fs
 * @return vrátí index volného datového bloku nebo nulu
 */
int32_t getFreeDataIndex(FileSystem *fileSystem);

/**
 * vytvoří nový prázdný adresář
 * @param fileSystem instance fs
 * @param freeInodeIndex freeInodeIndex
 * @param parentIndex parentIndex
 * @param name jméno adresáře
 * @return úspěch?
 */
bool makeEmptyDir(FileSystem *fileSystem, int32_t freeInodeIndex, int32_t parentIndex, const std::string& name);


/**
 * pokusí se přidat adresář do nadřazeného adresáře
 * @param fileSystem instance fs
 * @param dir daný adresář
 * @param parentIndex inode ID rodiče
 * @return true pokud se povedlo
 */
bool addDirectoryToParrent(FileSystem *fileSystem, directory_item &dir, int32_t parentIndex);

/**
 * načte inode z disku
 * @param fileSystem instance fs
 * @param nodeId id
 * @return inode
 */
pseudo_inode loadInode(FileSystem *fileSystem, int32_t nodeId);

/**
 * vypíše plné indexy u bitmapy dat nebo inodů
 * @param fileSystem instance fs
 * @param addr  bitmapa dat nebo inody
 */
void printFullBitIndexes(FileSystem *fileSystem, int32_t addr);

/**
 * je prázdný adresář?
 * @param fileSystem instance fs
 * @param inode inode adresáře
 * @param file čtení ze souboru
 * @return je prázdný?
 */
bool isDirEmpty(FileSystem *fileSystem, pseudo_inode &inode, std::fstream &file);

/**
 * smaže inode v souboru
 * @param fileSystem instance fs
 * @param file soubor
 * @param inodeToRemove daný inode
 * @return uúpěch?
 */
bool removeInode(FileSystem *fileSystem, std::fstream &file, pseudo_inode &inodeToRemove);

/**
 * odstraní odkaz na adresář/soubor v nadřeazeném adresáři
 * @param fileSystem instance fs
 * @param file zápis
 * @param inode daný inode
 * @param parent daný nadřazený inode
 */
void removeDirectoryItFromParrent(FileSystem *fileSystem, std::fstream &file, pseudo_inode &inode, pseudo_inode &parent);

/**
 * odstraní odkaz na adresář/soubor v nadřazeném adresáři
 * @param fileSystem fs
 * @param file stream
 * @param parent daný inode adresáře
 * @param name jméno (unsafe)
 */
void removeDirectoryIt(FileSystem *fileSystem, std::fstream &file, pseudo_inode &parent, std::string &name);

/**
 * odstraní data daného inodu
 * @param fileSystem instance fs
 * @param file zápis
 * @param inode daný inode
 */
void removeDataBlocks(FileSystem *fileSystem, std::fstream &file, pseudo_inode &inode);

/**
 * najde jméno daného souboru?
 * @param fileSystem instance fs
 * @param addresses dodané adresy na disku
 * @param name soubor
 * @return 0 pokud nenajde, pokud najde tak číslo inode
 */
int32_t findFileName(FileSystem *fileSystem, const std::vector<int32_t>& addresses, std::string& name);

/**
 * getter
 * @param fileSystem instance fs
 * @return počet prázdných databloků
 */
int32_t getFreeDataCount(FileSystem *fileSystem) ;

/**
 * zapíše do soubor do FS
 * @param fileSystem instance fs
 * @param source zdroj
 * @param file otevřený FS
 * @param freeInodeI volný inode index
 * @param srcSize velikost zdroje
 */
void writeData(FileSystem *fileSystem, const std::string& source, std::fstream &file, int32_t freeInodeI, double srcSize);

/**
 * zapíše datablock
 * @param fileSystem instance fs
 * @param src zdroj
 * @param file FS
 * @param stop je zapsáno? (FileSystem *fileSystem ano = 1, ne = 0)
 * @return adresa ve FS
 */
int32_t writeToDatablock(FileSystem *fileSystem,  std::ifstream &src, std::fstream &file, int8_t &stop);

/**
 * vytvoří datablock s nepřímých odkazů
 * @param fileSystem instance fs
 * @param file fs
 * @return adresa ve fs
 */
int32_t createIndirectDatab(FileSystem *fileSystem, std::fstream &file);

/**
 * zkopíruje data
 * @param fileSystem instance fs
 * @param addressesToCopy adresy
 * @param newInode nový inode
 */
void copyData(FileSystem *fileSystem, const std::vector<int32_t>& addressesToCopy, pseudo_inode &newInode);

/**
 * zkopíruje datablock do volného datablocku
 * @param fileSystem instance fs
 * @param addrToCopy původní adresa
 * @param file stream
 * @return nová adresa
 */
int32_t copyDataBlock(FileSystem *fileSystem, int32_t addrToCopy, std::fstream &file);

/**
 * odstraní odkaz na adresář/soubor v nadřeazeném adresáři
 * @param fileSystem instance fs
 * @param file zápis
 * @param inode daný inode
 * @param parent daný nadřazený inode
 */
void removeDirectoryItFromParrent(FileSystem *fileSystem,  pseudo_inode &inode, pseudo_inode &parent);

#endif //ZOS_FS_UTILS_H
